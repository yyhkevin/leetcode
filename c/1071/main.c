#include <stdlib.h>
#include <string.h>

int	len(char *str)
{
	int	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char	if_ok(char *str1, char *str2)
{
	int	size = len(str1) + len(str2) + 1;
	char	*buf = calloc(size, sizeof(char));
	char	*buf2 = calloc(size, sizeof(char));
	char	result;

	strcpy(buf, str1);
	strcat(buf, str2);
	strcpy(buf2, str2);
	strcat(buf2, str1);
	result = strcmp(buf, buf2);
	free(buf);
	free(buf2);
	return (result == 0);
}

char	*gcdOfStrings(char *str1, char *str2)
{
	if (if_ok(str1, str2))
	{

	}
	else
		return "";
}

#include <stdio.h>
int	main(void)
{
	printf("%d\n", if_ok("AAB", "AB"));
	return (0);
}
