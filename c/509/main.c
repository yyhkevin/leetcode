int fib(int n){
    int a = 0, b = 1;
    int temp;

    if (n < 2) return n;

    for (int i = 2; i <= n; i++) {
        temp = b;
        b = a + b;
        a = temp;
    }
    return b;
}
