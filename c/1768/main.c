#include <stdlib.h>

int	len(char *str)
{
	int	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char	*mergeAlternately(char *word1, char *word2)
{
	int counter = 0;
	char *final = malloc((1 + len(word1) + len(word2)) * sizeof(char));

	while (*word1 || *word2)
	{
		if (*word1)
			final[counter++] = *word1++;
		if (*word2)
			final[counter++] = *word2++;
	}
	final[counter] = 0;
	return (final);
}

#include <stdio.h>
int	main(void)
{
	char* result = mergeAlternately("hello", "longworld");
	printf("%s\n", result);
}
