/*
This question could be easier / made less confusing, when thinking of `nums` as a list of characters.

Observe that items could be placed in a circular fashion consecutively.
The minimum seconds required would be the minimum of the different maximums of distances between the same item.
Consider two items in indexes i and j:
1. the distance between them is j - 1, or (i+n) - j, where n is the total number of elements
2. the required number of steps is the distance divided by two as both elements could progress towards each other ("spread across the circle")
3. any items between the arc made by i and j would have a smaller distance
*/
var minimumSeconds = function(nums) {
    const ref = new Map();
    // first get a map containing entries of the character - list of indexes of that character
    for (const [i, num] of nums.entries()) {
        let list = ref.get(num);
        if (list === undefined) list = [];
        list.push(i);
        ref.set(num, list);
    }
    // could be infinity, but since the constraint is for n to be up to 10^5, divide by 2
    let minSeconds = 50000;
    for (const list of ref.values()) {
        // for each character, append the first index added with the total length for it to calculate the distance between the last and the first item
        list.push(list[0] + nums.length);
        let dist = 0;
        for (let i = 1; i < list.length; i++) {
            // calculate the distance between any two consecutive indexes of the same character, and take the worst case scenario
            dist = Math.max(dist, Math.floor((list[i] - list[i-1])/2));
        }
        // finally, get the minimum distance required across all characters
        minSeconds = Math.min(minSeconds, dist);
    }
    return minSeconds;
};
