var bestClosingTime = function(customers) {
    let maxCustomers = 0;
    let count = 0;
    let bestClosing = 0;
    for (let i = 0; i < customers.length; i++) {
        if (customers[i] === 'Y') {
            count++;
            if (count > maxCustomers) {
                maxCustomers = count;
                bestClosing = i + 1;
            }
        } else {
            count--;
        }
    }
    return bestClosing;
};
