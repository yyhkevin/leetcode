var isPalindrome = function(s) {
    const lower = s.toLowerCase();
    const str = lower.replaceAll(/[^a-z0-9]/g, '');
    for (let i = 0; i < (str.length << 1); i++) {
        if (str[i] !== str[str.length - 1 - i]) return false;
    }
    return true;
};
