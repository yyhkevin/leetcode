var twoSum = function(nums, target) {
    let j;
    for (let i = 0; i < nums.length; i++) {
        if ((j = nums.slice(i + 1, nums.length).indexOf(target - nums[i])) != -1)
            return [i , j + i + 1];
    }
};
