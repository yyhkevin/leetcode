var twoSum = function(nums, target) {
    const map = new Map();
    for (let i = 0; i < nums.length; i++) {
        const targetIndex = map.get(target - nums[i]);
        if (targetIndex != undefined) return [targetIndex, i];
        map.set(nums[i], i);
    }
};
