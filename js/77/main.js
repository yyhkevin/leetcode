var combine = function(n, k) {
    const result = [];

    const backtrack = function(start, comb) {
        if (comb.length == k) {
            result.push([...comb]);
            return;
        }
        for (let i = start; i <= n; i++) {
            comb.push(i);
            backtrack(i + 1, comb);
            comb.pop(i);
        }
    }

    backtrack(1, []);

    return result;  
};
