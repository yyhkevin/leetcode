var getTargetCopy = function(original, cloned, target) {
    const stack = [original];
    const stackClone = [cloned];
    while (stack.length > 0) {
        const node = stack.pop();
        const nodeClone = stackClone.pop();
        if (node === target) return nodeClone;
        if (node.left !== null) {
            stack.push(node.left);
            stackClone.push(nodeClone.left);
        }
        if (node.right != null) {
            stack.push(node.right);
            stackClone.push(nodeClone.right);
        }
    }
};
