var isPalindrome = function(head) {
    let slow = head;
    let fast = head;

    // [fast] because itself could be null
    // [fast.next] to ensure it can go to the next fast pointer
    while (fast && fast.next) {
        fast = fast.next.next;
        slow = slow.next;
    }

    let prev = null;
    // reverse order for second half of list
    while (slow) {
        const temp = slow.next;
        slow.next = prev;
        prev = slow;
        slow = temp;
    }

    let left = head;
    // the ending [prev] will be the last item in the list
    let right = prev;

    while (right) {
        if (left.val !== right.val) return false;
        left = left.next;
        right = right.next;
    }
    return true;
};
