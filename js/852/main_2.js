/**
 * @param {number[]} arr
 * @return {number}
 */
var peakIndexInMountainArray = function(arr) {
    let i = 0;
    while (i + 1 < arr.length && arr[i + 1] > arr[i]) i++;
    return i;
};
