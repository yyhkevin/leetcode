/**
 * @param {number[]} arr
 * @return {number}
 */
var peakIndexInMountainArray = function(arr) {
    let i = -1;
    let max = arr[0];
    let j = 1;

    while(j < arr.length) {
        if (arr[j] > max) {
            i = j;
            max = arr[j];
        } else {
            break;
        }
        j++;
    }
    return i;
};
