/**
 * @param {number[]} arr
 * @return {number}
 */

function div(a , b) {
    return Math.floor(a / b);
}

var peakIndexInMountainArray = function(arr) {
    let left = 0 ;
    let right = arr.length - 1;
    while (left != right) {
        let mid = div(left + right, 2);
        if (arr[mid - 1] < arr[mid] && arr[mid] > arr[mid + 1]) return mid;
        if (arr[mid - 1] < arr[mid] && arr[mid] < arr[mid + 1]) {
            left = mid;
        } else {
            right = mid;
        }
    }
    return left;
};
