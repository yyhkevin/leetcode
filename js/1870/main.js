/**
 * @param {number[]} dist
 * @param {number} hour
 * @return {number}
 */
var minSpeedOnTime = function(dist, hour) {
    let left = 1;
    let right = 10000000;
    let min = -1;

    let time;
    while (left <= right) {
        time = 0;
        let middle = Math.floor((left + right) / 2);
        for (let i = 0; i < dist.length - 1; i++) {
            time += Math.ceil(dist[i] / middle);
        } 
        time += dist[dist.length - 1] / middle;
	// The following condition is important: NOT time < hour
	// The <= ensures that the current speed is sufficient, and there could potentially be smaller speeds
        if (time <= hour) right = middle - 1;
        else left = middle + 1;
        if (time <= hour) min = middle;
    }
    return min;
};
