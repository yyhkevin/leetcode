var permute = function(nums) {
    const result = [];

    const backtrack = function(perm, used) {
        if (perm.length === nums.length) {
            result.push([...perm]);
            return;
        }
        for (let i = 0; i < nums.length; i++) {
            if (used[i]) continue;
            perm.push(nums[i]);
            used[i] = true;
            backtrack(perm, used);
            perm.pop();
            used[i] = false;
        }
    }

    backtrack([], []);

    return result;
};
