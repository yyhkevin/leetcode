
/*
Solution is to go from bottom right, then to bottom left, and upwards. Hence from bottom right to top left.

s1 is SEA and s2 is EAT.
Row: i, Column: j

|   | E | A | T | |
| S |
| E |
| A |
|   |


Smallest sub problem is A vs T which is i = 2 and j = 2.
Next step is A vs AT which is i = 2 and j = 1.

The idea is to turn s1 into s2 through Insert and Delete.

The numbers in each cell represent the minimum cost (ASCII sum) required to solve that subproblem.

For example, look at j = 2 column: it is "" vs "", A vs "", EA vs "" and SEA vs "". It requires deletion.
The cost will therefore be 0, A, E+A, S+E+A:

|   | E | A | T |     |
| S |           | 313 |
| E |           | 198 |
| A |           | 97  |
|   |           | 0   |

Similarly for i = 2 row: it is "" vs "", "" vs T, "" vs AT, "" vs EAT. It requires insertion.
|   | E   | A   | T   |     |
| S |                 | 313 |
| E |                 | 198 |
| A |                 | 97  |
|   | 314 | 213 | 116 | 0   |

From the bottom right to top left, for each cell there are three cases:
1. s1[i] === s2[j]
This means that no operation is needed. Just obtain the cost of the next subproblem which is at [i + 1, j + 1].

2. Insert to s1 the first character of s2 to make its first character same, equivalently moving the s2 pointer to its next position.
This adds a cost of s2.charCodeAt(j), to its next subproblem which is at [i, j + 1].

3. Delete the first character of s1, moving the s1 pointer to its next position.
This adds a cost of s1.charCodeAt(i), to its next subproblem which is at [i + 1, j].

If it is not case 1, to determine the current cell value, both 2 and 3 must be calculated and the minimum value is set.

The process is repeated until the values of all cells are calculated. The value at cache[0][0] (the main subproblem) is returned.
*/
var minimumDeleteSum = function(s1, s2) {
    let cache = [];

    // Initialize DP table (top left corner is the full problem) with infinity cost for every cell
    for (let i = 0; i <= s1.length; i++) {
        let row = [];
        for (let j = 0; j <= s2.length; j++) {
            row.push(Infinity);
        }
        cache.push(row);
    }

    // Bottom right is comparison between "" and "", which requires no cost
    cache[s1.length][s2.length] = 0;

    // Set the values at the last column equal to the cost of the deletion costs.
    for (let i = s1.length - 1; i >= 0; i--) {
        cache[i][s2.length] = cache[i + 1][s2.length] + s1.charCodeAt(i);
    }

    // Vice versa for insertion costs.
    for (let i = s2.length - 1; i >= 0; i--) {
        cache[s1.length][i] = cache[s1.length][i + 1] + s2.charCodeAt(i);
    }

    for (let i = s1.length - 1; i >= 0; i--) {
        for (let j = s2.length - 1; j >= 0; j--) {
            if (s1[i] === s2[j]) {
                // No cost needed; just refer to its diagonal cell
                cache[i][j] = cache[i+1][j+1];
            } else {
                // The minimum between an Insertion vs Deletion operation
                cache[i][j] = Math.min(s2.charCodeAt(j) + cache[i][j + 1], s1.charCodeAt(i) + cache[i + 1][j]);
            }
        }
    }

    return cache[0][0];
};
