var flat = function (arr, n) {
    if (n === 0) return arr;
    const answer = [];
    for (let i = 0; i < arr.length; i++) {
        if (n === 0 || (typeof arr[i]) === "number") {
            answer.push(arr[i]);
        } else {
            answer.push(...flat(arr[i], n - 1));
        }
    }
    return answer;
};
