Array.prototype.groupBy = function(fn) {
    const output = {};
    for (let i = 0; i < this.length; i++) {
        const key = fn(this[i]);
        if(!output.hasOwnProperty(key)) output[key] = [];
        output[key].push(this[i]);
    }
    return output;
};
