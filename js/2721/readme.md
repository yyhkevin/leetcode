Learning points:

1. No async + await can be used as the processes must be done in parallel, hence the `then` construct is used
2. `arr.push` cannot be used as the results must be in order, hence `arr[i] = result` is used
3. `resolve` cannot be put after the `for` loop as the `then` construct is used - it will be executed immediately and resolves an empty value, hence a counter is used to count how many successful operations and resolve only when all is resolved
