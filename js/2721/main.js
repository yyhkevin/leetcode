var promiseAll = async function(functions) {
    return new Promise((resolve, reject) => {
        let result = [];
        let count = 0;
        for (let i = 0; i < functions.length; i++) {
            functions[i]().then(r => {
                result[i] = r;
                count++;
                if (count == functions.length) resolve(result);
            }).catch(e => reject(e));
        }
    });
};
