var chunk = function(arr, size) {
    const chunks = [];
    for (let i = 0; i < Math.ceil(arr.length / size); i++) {
        let chunk = arr.slice(i * size, (i+1) * size);
        chunks.push(chunk);
    }
    return chunks;
};
