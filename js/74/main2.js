var searchMatrix = function(matrix, target) {
    const len = matrix[0].length;

    let top = 0;
    let bot = matrix.length - 1;

    while (top <= bot) {
        const mid = top + ((bot - top) >> 1);
        if (target > matrix[mid][len - 1]) top = mid + 1;
        else if (target < matrix[mid][0]) bot = mid - 1;
        else break;
    }

    if (top > bot) return false;

    let left = 0;
    let right = len - 1;
    
    const row = top + ((bot - top) >> 1);
    while (left <= right) {
        const mid = left + ((right - left) >> 1);
        if (target === matrix[row][mid]) return true;
        if (target > matrix[row][mid]) left = mid + 1;
        else right = mid - 1;
    }

    return false;
};
