var searchMatrix = function(matrix, target) {
    let row = 0;
    let len = matrix[0].length;
    while (row < matrix.length) {
        if (target <= matrix[row][len - 1]) {
            let left = 0;
            let right = len - 1;
            while (left <= right) {
                const mid = left + Math.floor((right - left) / 2);
                if (target === matrix[row][mid]) return true;
                if (target > matrix[row][mid]) left = mid + 1;
                else right = mid - 1;
            }
            return false;
        }
        row++;
    }
    return false;
};
