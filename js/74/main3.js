var searchMatrix = function(matrix, target) {
    const len = matrix.length * matrix[0].length;
    let left = 0;
    let right = len - 1;

    while (left <= right) {
        const mid = left + ((right - left) >> 1);
        const midV = matrix[Math.floor(mid / matrix[0].length)][mid % matrix[0].length];
        if (target === midV) return true;
        if (target > midV) left = mid + 1;
        else right = mid - 1;
    }
    return false;
};
