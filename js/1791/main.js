var findCenter = function(edges) {
    const set = new Set();
    for (const [e1,e2] of edges) {
        if(set.has(e1)) return e1;
        if(set.has(e2)) return e2;
        set.add(e1);
        set.add(e2);
    }
};
