/**
 * @param {number} x
 * @param {number} n
 * @return {number}
 */
var myPow = function(x, n) {
    const a = n < 0 ? -n : n;
    if (n === 0) return 1;
    let temp = myPow(x, Math.floor(a / 2));
    temp *= temp;
    if (a % 2 === 1) {
        temp *= x;
    }
    if (n < 0) temp = 1 / temp;
    return temp;
}
