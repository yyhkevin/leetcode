/**
 * @param {number} x
 * @param {number} n
 * @return {number}
 */
var myPow = function(x, n) {
    if (x === 1.0) return 1;
    const a = n < 0 ? -n : n;
    if (x === -1.0) return (a % 2 == 0 ? 1 : -1)
    let val = 1;
    for (let i = 0; i < a; i++) val *= x;
    if (n < 0) val = 1 / val;
    return val;
};
