/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function(s) {
    const mapping = {
        '{': '}',
        '(': ')',
        '[': ']',
    };
    const stack = [];
    for (const c of s) {
        if (mapping[c]) {
            stack.push(mapping[c]);
            continue;
        }
        if (c === ']' || c === ')' || c === '}') {
            if (stack[stack.length - 1] === c) {
                stack.pop();
            } else {
                return false;
            }
        }
    }
    return stack.length === 0;
};
