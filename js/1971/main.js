var validPath = function(n, edges, source, destination) {
    const adj = new Map();
    for (const [e1, e2] of edges) {
        const e1p = adj.get(e1);
        if(e1p === undefined) {
            adj.set(e1, []);
        }
        const e2p = adj.get(e2);
        if(e2p === undefined) {
            adj.set(e2, []);
        }
        adj.get(e1).push(e2);
        adj.get(e2).push(e1);
    }
    const stack = [source];
    const seen = new Set();
    while (stack.length > 0) {
        const node = stack.pop();
        seen.add(node);
        if (node === destination) return true;
        for (const neighbour of adj.get(node)) {
            if(!seen.has(neighbour)) stack.push(neighbour);
        }
    }
    return false;
};
