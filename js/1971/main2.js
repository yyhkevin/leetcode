/*
Uses BFS with Queue rather than DFS with Stack
The `seen` should be added as nodes are added to the queue, rather than added on each run to reduce total items in queue.
*/
var validPath = function(n, edges, source, destination) {
    const adj = new Map();
    for (const [e1, e2] of edges) {
        const e1p = adj.get(e1);
        if(e1p === undefined) {
            adj.set(e1, []);
        }
        const e2p = adj.get(e2);
        if(e2p === undefined) {
            adj.set(e2, []);
        }
        adj.get(e1).push(e2);
        adj.get(e2).push(e1);
    }
    const q = [source];
    const seen = new Set();
    seen.add(source);
    while (q.length > 0) {
        const node = q.shift();
        if (node === destination) return true;
        for (const neighbour of adj.get(node)) {
            if (!seen.has(neighbour)) {
                q.push(neighbour);
                seen.add(neighbour);
            }
        }
    }
    return false;
};
