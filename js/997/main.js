var findJudge = function(n, trust) {
    const trustC = {};
    for (let i = 1; i <= n; i++) trustC[i] = 0;
    for (const [a,b] of trust) {
        trustC[a]--;
        trustC[b]++;
    }
    for (let i in trustC) {
        if (trustC[i] === n-1) return i;
    }
    return -1;
}
