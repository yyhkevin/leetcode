var findJudge = function(n, trust) {
    const count = Array(n).fill(0);
    for (const [p, j] of trust) {
        count[p-1]--;
        count[j-1]++;
    }
    for (const [index, value] of count.entries()) {
        if (value === n-1) return index + 1;
    }
    return -1;
}
