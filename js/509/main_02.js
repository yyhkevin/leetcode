var fib = function(n) {
    let memoized = [0, 1, 1];
    for (let i = 3; i <= n; i++) {
        memoized.push(memoized[i - 1] + memoized[i - 2]);
    }
    return memoized[n];
};

