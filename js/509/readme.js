Fibonacci

### Weird leetcode issue:

In main_01.js, the `memoized` array is declared on the global scope. On LeetCode test, the result is weird (see 01_3.png to 01_5.png); local NodeJS environment registers correct result.

After moving the array into the function scope in main_02.js, it works as expected.

### Solved:

It is because of global definition of the `memoized` array. It is not reset on each test, and it retains its value from the previous test. By putting it within the scope of the function, it will work correctly.

### main_03.js

It is done without memoization via an array.
