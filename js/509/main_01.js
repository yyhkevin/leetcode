let memoized = [0, 1, 1];

var fib = function(n) {
    for (let i = 3; i <= n; i++) {
        memoized.push(memoized[i - 1] + memoized[i - 2]);
    }
    return memoized[n];
};

console.log(fib(10));
