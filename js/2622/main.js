var TimeLimitedCache = function() {
    this.cache = new Map();
    this.timers = new Map();
};

TimeLimitedCache.prototype.set = function(key, value, duration) {
    const found = this.cache.get(key);
    if (found != undefined) {
        clearTimeout(this.timers.get(key));
    }
    this.timers.set(key, setTimeout(() => this.cache.delete(key), duration));
    this.cache.set(key, value);
    return found != undefined;
};

TimeLimitedCache.prototype.get = function(key) {
    const found = this.cache.get(key);
    return found == undefined ? -1 : found;
};

TimeLimitedCache.prototype.count = function() {
    return this.cache.size;
};
