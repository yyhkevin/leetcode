var addTwoNumbers = function(l1, l2) {
    let sum = l1.val + l2.val;
    let carry = Math.floor(sum / 10);
    let prev = new ListNode(sum % 10);
    const head = prev;
    l1 = l1.next;
    l2 = l2.next;
    while (l1 || l2 || carry != 0) {
        let sum = (l1?.val ?? 0) + (l2?.val ?? 0) + carry;
        carry = Math.floor(sum / 10);
        const curr = new ListNode(sum % 10);
        prev.next = curr;
        prev = curr;
        l1 = l1?.next;
        l2 = l2?.next;
    }

    return head;
};
