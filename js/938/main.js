var rangeSumBST = function(root, low, high) {
    const stack = [root];
    let sum = 0;
    while (stack.length > 0) {
        const node = stack.pop();
        if (node.val >= low && node.val <= high) sum+=node.val;
        if(node.val > low && node.left !== null) stack.push(node.left);
        if(node.val < high && node.right != null) stack.push(node.right);
    }

    return sum;
};
