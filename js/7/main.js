var reverse = function(x) {
    let reversed = 0;
    let isNegative = x < 0;

    if(isNegative) x *= -1;

    while (x) {
        reversed *= 10;
        reversed += x % 10;
        x = Math.floor(x/10);
    }

    if(isNegative) reversed *= -1;

    if (reversed > 2147483647 || reversed < -2147483648) return 0;

    return reversed;
};
