/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLongestSubstring = function(s) {
    let longest = 0;
    const seen = new Set();
    let start = 0;
    let end = 0;

    while (end < s.length) {
        if (seen.has(s[end])) {
            seen.delete(s[start]);
            start++;
        } else {
            seen.add(s[end]);
            end++;
            longest = Math.max(longest, end - start);
        }
    }

    return longest;
};
