var canPlaceFlowers = function(flowerbed, n) {
    for (let i = 0; i < flowerbed.length; i++) {
        // current is available, left and right is not yet taken
        // for out of bounds, it is undefined => !undefined => true
        // if 0, then !0 => 1
        if (flowerbed[i] === 0 && !flowerbed[i-1] && !flowerbed[i+1]) {
            n--;
            flowerbed[i] = 1;
        }
    }
    return n <= 0;
};
