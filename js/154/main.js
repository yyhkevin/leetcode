var findMin = function(nums) {
    let min = nums[0];
    let left = 0;
    let right = nums.length - 1;
    
    while (left <= right) {
        // If array is already sorted, take the left most value
        if (nums[left] < nums[right]) {
            if (nums[left] < min) min = nums[left];
            break;
        }
        const mid = left + ((right - left) >> 1);
        if (nums[mid] < min) min = nums[mid];
        // If left most value is same as middle value, unable to determine which part of the rotated sorted array we are at
        // [2, 1, 2, 2, 2] vs [2, 2, 2, 1, 2]
        if (nums[left] === nums[mid]) left++;
        else if (nums[left] < nums[mid]) left = mid + 1;
        else right = mid - 1;
    }
    return min;
};
