var search = function(nums, target) {
    let left = 0;
    let right = nums.length - 1;

    while (left <= right) {
        let middle = left + Math.floor((right - left) / 2);
        if (nums[middle] === target) return middle;
        // because of the nature of the sort (increasing), ensure <=
        // < will fail in case of [3,1]
        // strictly increasing case on left side:
        if (nums[left] <= nums[middle]) {
            // if target lies between left and middle
            if (target >= nums[left] && target <= nums[middle]) {
                right = middle - 1;
            } else {
                left = middle + 1;
            }
        // strictly increasing case on right side:
        } else {
            // if target lies between middle and right
            if (target >= nums[middle] && target <= nums[right]) {
                left = middle + 1;
            } else {
                right = middle - 1;
            }
        }
    }
    return -1;
};
