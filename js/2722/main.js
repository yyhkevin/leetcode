var join = function(arr1, arr2) {
    const result = new Map();
    for (let i = 0; i < arr1.length; i++) result.set(arr1[i].id, arr1[i]);
    for (let i = 0; i < arr2.length; i++) {
        if (result.has(arr2[i].id)) {
            const existingObj = result.get(arr2[i].id);
            for (const prop in arr2[i]) {
                existingObj[prop] = arr2[i][prop]
            }
        } else {
            result.set(arr2[i].id, arr2[i]);
        }
    }

    const output = [...result.values()];
    output.sort((a,b) => a.id - b.id);
    return output;
};
