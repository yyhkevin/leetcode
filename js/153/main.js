var findMin = function(nums) {
    let min = Infinity;
    let left = 0;
    let right = nums.length - 1;

    while (left <= right) {
        const mid = left + ((right - left) >> 1);
        const midV = nums[mid];
        if (midV < min) min = midV;
        if (nums[left] < nums[right]) {
            if (nums[left] < min) min = nums[left];
            break;
        }
        if (nums[left] <= nums[mid]) left = mid + 1;
        else right = mid - 1;
    }

    return min;
};
