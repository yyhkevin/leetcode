var validPalindrome = function(s) {
    let left = 0;
    let right = s.length - 1;
    while (left < right) {
        if (s[left] !== s[right]) {
            const rhs = s.slice(left + 1, right + 1);
            const lhs = s.slice(left, right);
            return lhs === lhs.split('').reverse().join('') || rhs === rhs.split('').reverse().join('');
        }
        left++;
        right--;
    }
    return true;
};
