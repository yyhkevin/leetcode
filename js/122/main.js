/*
The idea is that differences in small prices, when added up will be greater than the profit from highest - lowest.
As long as today is more expensive than yesterday, sell and take profit.
As long as today is cheaper, buy.
*/
var maxProfit = function(prices) {
    let profit = 0;
    let price = prices[0];

    for (let i = 1; i < prices.length; i++) {
        // set price so far to be today if its cheapest that we have seen, ever since any stock was last sold
        if (prices[i] < price) price = prices[i];
        else {
            // take profit if its more expensive today
            profit += prices[i] - price;
            // make sure to update that we no longer hold this price as we've bought
            price = prices[i];
        }
    }

    return profit;
};
